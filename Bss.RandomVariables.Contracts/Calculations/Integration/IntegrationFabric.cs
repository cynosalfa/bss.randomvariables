﻿using System;
using Bss.RandomVariables.Contracts.Enums;

namespace Bss.RandomVariables.Contracts.Calculations.Integration
{
    /// <summary>
    /// Фабрика методов интеграции
    /// </summary>
    public static class IntegrationFabric
    {
        /// <summary>
        /// Создаёт экземпляр метода по значению перечисления
        /// </summary>
        /// <param name="value">Значение перечисления</param>
        /// <returns>Экземпляр класса метода интегрирования</returns>
        public static IIntegration CreateIntegrator(this IntegrationMethods value)
        {
            switch (value)
            {
                    case IntegrationMethods.LeftRectangles:
                    return new LeftRectangles();

                    case IntegrationMethods.MiddleRectangles:
                    return new MiddleTriangles();

                    case IntegrationMethods.RightReactangles:
                    return new RightTriangles();

                    case IntegrationMethods.Trapezoid:
                    return new Trapezoid();

                    case IntegrationMethods.Simpson:
                    return new Simpson();

                    default: throw new Exception(ErrorsEnum.UnexpectedEnumError.GetDescriptionFromEnumValue());
            }
        }
    }
}