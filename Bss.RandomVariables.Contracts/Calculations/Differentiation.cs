﻿using System.Collections.Generic;
using Bss.RandomVariables.Contracts.HelperClasses;

namespace Bss.RandomVariables.Contracts.Calculations
{
    /// <summary>
    /// Осуществляет операции взятия производной
    /// </summary>
    public static class Differentiation
    {
        /// <summary>
        /// Вычисляет производную
        /// </summary>
        /// <param name="distribution">Таблично заданный закон распределения</param>
        /// <returns>Таблично заданную плотность распределения</returns>
        public static List<TableValue> TakeDerivative(this List<TableValue> distribution)
        {
            var derivativeTable = new List<TableValue>();
            for (int i = 0; i < distribution.Count - 1; i++)
            {
                var derivativeValue = 
                    (distribution[i + 1].ValueY - distribution[i].ValueY)/
                    (distribution[i + 1].ValueX - distribution[i].ValueX);

                derivativeTable.Add(new TableValue(distribution[i].ValueX, derivativeValue));
            }

            return derivativeTable;
        }
    }
}