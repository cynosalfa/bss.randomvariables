﻿using System;
using System.Collections.Generic;
using System.Linq;
using Bss.RandomVariables.Contracts.Enums;

namespace Bss.RandomVariables.Contracts.HelperClasses
{
    /// <summary>
    /// Производит сканирование делегата с плавающим интервалом
    /// </summary>
    public class DelegateScanner
    {
        private readonly double _stepY;
        private double _stepX;
        private double Epsilon
        {
            get { return _stepX * 0.01d; }
        }

        private readonly Func<double, double> _func;
        private readonly Interval _scanningInterval;
        private readonly List<TableValue> _table;
        private readonly bool _isSelection;

        /// <summary>
        /// Просканированный в таблицу делегат
        /// </summary>
        public List<TableValue> Table { get { return _table; } }

        /// <summary>
        /// Конструкто инициализации
        /// </summary>
        /// <param name="step">Шаг сканировани</param>
        /// <param name="scanningInterval">Интервал сканирования</param>
        /// <param name="func">Делегат</param>
        /// <param name="isSelection">Получить выборку</param>
        public DelegateScanner(double step, Interval scanningInterval, Func<double, double> func, bool isSelection = false)
        {
            _func = func;
            _scanningInterval = scanningInterval;
            _stepX = step;
            _stepY = step;
            _table = new List<TableValue>();
            _isSelection = isSelection;
        }

        /// <summary>
        /// Осуществляет сканирование делегата
        /// </summary>
        public void Scan()
        {
            _table.Add(new TableValue(_scanningInterval.LeftEdge, _func(_scanningInterval.LeftEdge)));

            var leftEdge = _scanningInterval.LeftEdge;
            var rightEdge = _scanningInterval.LeftEdge + _stepX;

            while (rightEdge < _scanningInterval.RightEdge)
            {
                var funcSign = _func(leftEdge) - _func(rightEdge);
                var direction = FunctionDirection.Increase;
                
                double searchValue = _table.Last().ValueY;

                if (Math.Abs(funcSign) > Configuration.HighAccuracy)
                {
                    if (funcSign > 0)
                    {
                        searchValue = _table.Last().ValueY - _stepY;
                        direction = FunctionDirection.Decrease;
                    }
                    else if (funcSign < 0)
                    {
                        searchValue = _table.Last().ValueY + _stepY;
                    }
                }
                else
                {
                    _table.Add(new TableValue(rightEdge, _func(rightEdge)));
                    leftEdge = rightEdge;
                    rightEdge = leftEdge + _stepX;
                    continue;
                }

                if (!_isSelection)
                {
                    if (direction == FunctionDirection.Increase && searchValue > _func(rightEdge))
                    {
                        _table.Add(new TableValue(rightEdge, _func(rightEdge)));
                        leftEdge = rightEdge + Epsilon;
                        rightEdge = leftEdge + _stepX;

                        continue;
                    }

                    if (direction == FunctionDirection.Decrease && searchValue < _func(rightEdge))
                    {
                        _table.Add(new TableValue(rightEdge, _func(rightEdge)));
                        leftEdge = rightEdge + Epsilon;
                        rightEdge = leftEdge + _stepX;

                        continue;
                    }
                }

                var foundValue = GetNextStep(new SearchItem(leftEdge, rightEdge, searchValue), direction);

                _table.Add(new TableValue(foundValue, _func(foundValue)));

                _stepX = Math.Abs(leftEdge - foundValue);

                leftEdge = foundValue + Epsilon;
                rightEdge = leftEdge + _stepX;
            }

            _table.Add(new TableValue(_scanningInterval.RightEdge, _func(_scanningInterval.RightEdge)));
        }

        /// <summary>
        /// Осуществляет поиск следующего шага для считывания значения
        /// </summary>
        /// <param name="item">Искомое значение в интервале</param>
        /// <param name="direction">Знак производной</param>
        /// <returns>Следующий шаг сканирования</returns>
        public double GetNextStep(SearchItem item, FunctionDirection direction)
        {
            double root = 0;
            bool rootFound = false;

            bool isThereRoot = item.IsThereRoot(_func);

            while (!isThereRoot)
            {
                item.RightEdge = item.RightEdge + _stepX;
                isThereRoot = item.IsThereRoot(_func);
            }
            
            while (!rootFound)
            {
                root = (item.RightEdge + item.LeftEdge) / 2;

                var delta = _func(root) - item.SearchValue;

                switch (direction)
                {
                        case FunctionDirection.Increase:
                        if (Math.Abs(delta) > Epsilon)
                        {
                            if (delta > 0)
                            {
                                item.RightEdge = root;
                            }
                            else
                            {
                                item.LeftEdge = root;
                            }
                        }
                        else
                        {
                            rootFound = true;
                        }
                        break;
                        case FunctionDirection.Decrease:
                        if (Math.Abs(delta) > Epsilon)
                        {
                            if (delta < 0)
                            {
                                item.RightEdge = root;
                            }
                            else
                            {
                                item.LeftEdge = root;
                            }
                        }
                        else
                        {
                            rootFound = true;
                        }
                        break;
                }
            }

            return root;
        }
    }
}