﻿using System.ComponentModel;

namespace Bss.RandomVariables.Contracts.Enums
{
    /// <summary>
    /// Типы ошибок и их описания
    /// </summary>
    public enum ErrorsEnum
    {
        /// <summary>
        /// Неожиданное значение перечисления
        /// </summary>
        [Description("Неожиданное значение перечисления")] 
        UnexpectedEnumError,
        /// <summary>
        /// Неожиданное значение перечисления
        /// </summary>
        [Description("Ошибка задания случайной величины")]
        VariableDescriptionError
    }
}