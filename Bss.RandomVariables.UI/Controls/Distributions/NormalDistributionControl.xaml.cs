﻿using System.Windows.Controls;

namespace Bss.RandomVariables.UI.Controls.Distributions
{
    /// <summary>
    /// Контрол задания нормального распределения
    /// </summary>
    public partial class NormalDistributionControl : UserControl
    {
        /// <summary>
        /// Конструктор по умолчанию
        /// </summary>
        public NormalDistributionControl()
        {
            InitializeComponent();
        }
    }
}
