﻿using System;
using System.Windows.Input;

namespace Bss.RandomVariables.UI.Utils
{
    /// <summary>
    /// Реализация ICommand для связывания с элементами управления контролов
    /// </summary>
    public class DelegateCommand : ICommand
    {
        private readonly Predicate<object> _canExecute;
        private readonly Action<object> _execute;

        /// <summary>
        /// Событие изменения умловия выполнения делегата
        /// </summary>
        public event EventHandler CanExecuteChanged;

        /// <summary>
        /// Конструктор инициализации
        /// </summary>
        /// <param name="execute">Делегат для выполнения</param>
        public DelegateCommand(Action<object> execute)
            : this(execute, null)
        {
        }

        /// <summary>
        /// Конструктор инициализации
        /// </summary>
        /// <param name="execute">Делегат для выполнения</param>
        /// <param name="canExecute">Условие возможности выполнения делегата</param>
        public DelegateCommand(Action<object> execute,
                       Predicate<object> canExecute)
        {
            _execute = execute;
            _canExecute = canExecute;
        }

        public bool CanExecute(object parameter)
        {
            if (_canExecute == null)
            {
                return true;
            }

            return _canExecute(parameter);
        }

        public void Execute(object parameter)
        {
            _execute(parameter);
        }

        /// <summary>
        /// Взводит событие изменения условия выполнения делегата
        /// </summary>
        public void RaiseCanExecuteChanged()
        {
            if (CanExecuteChanged != null)
            {
                CanExecuteChanged(this, EventArgs.Empty);
            }
        }
    }
}