﻿using System;
using System.Collections.Generic;
using Bss.RandomVariables.Contracts.Calculations;
using Bss.RandomVariables.Contracts.HelperClasses;

namespace Bss.RandomVariables.Contracts.Extensions
{
    /// <summary>
    /// Расширения для списков
    /// </summary>
    public static class ListExtensions
    {
        /// <summary>
        /// Получает значение линейно-апроксимированной функции
        /// </summary>
        /// <param name="item">Таблица значений</param>
        /// <param name="valueX">Аргумент</param>
        /// <returns></returns>
        public static double GetUndescribedYValue(this List<TableValue> item, double valueX)
        {
            var coefficients = new Tuple<double, double>(0,0);

            if (valueX < item[0].ValueX)
            {
                return valueX * coefficients.Item1 + coefficients.Item2;
            }

            for (int i = 0; i < item.Count; i++)
            {
                
                if (valueX > item[i].ValueX)
                {
                    continue;
                }
                if (Math.Abs(valueX - item[i].ValueX) < Configuration.HighAccuracy)
                {
                    return item[i].ValueY;
                }

                coefficients = FunctionDetector.Linear(item[i], item[i - 1]);
                break;
            }

            return valueX * coefficients.Item1 + coefficients.Item2;
        }

        /// <summary>
        /// Получает значение линейно-апроксимированной функции
        /// </summary>
        /// <param name="item">Таблица значений</param>
        /// <param name="valueY">Аргумент</param>
        /// <returns></returns>
        public static double GetUndescribedXValue(this List<TableValue> item, double valueY)
        {
            var coefficients = new Tuple<double, double>(0, 0);

            if (valueY < item[0].ValueY)
            {
                return valueY * coefficients.Item1 + coefficients.Item2;
            }

            for (int i = 1; i < item.Count; i++)
            {
                if (valueY > item[i].ValueY)
                {
                    continue;
                }
                if (Math.Abs(valueY - item[i].ValueY) < Configuration.HighAccuracy)
                {
                    return item[i].ValueX;
                }

                coefficients = FunctionDetector.Linear(item[i], item[i - 1]);
                break;
            }

            return (valueY - coefficients.Item2) / coefficients.Item1;
        }
    }
}