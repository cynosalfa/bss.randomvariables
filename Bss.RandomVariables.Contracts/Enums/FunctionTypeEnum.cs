﻿using System.ComponentModel;

namespace Bss.RandomVariables.Contracts.Enums
{
    /// <summary>
    /// Типы делегатов, описывающих случайную величину
    /// </summary>
    public enum FunctionTypeEnum
    {
        /// <summary>
        /// Распределение
        /// </summary>
        [Description("Распределение (F)")]
        Distribution,
        /// <summary>
        /// Плотность
        /// </summary>
        [Description("Плотность (f)")]
        Density
    }
}