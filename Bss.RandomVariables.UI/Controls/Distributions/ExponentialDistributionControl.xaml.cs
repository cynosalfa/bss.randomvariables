﻿using System.Windows.Controls;

namespace Bss.RandomVariables.UI.Controls.Distributions
{
    /// <summary>
    /// Контрол задания экспоненцильного распределения
    /// </summary>
    public partial class ExponentialDistributionControl : UserControl
    {
        /// <summary>
        /// Конструктор по умолчанию
        /// </summary>
        public ExponentialDistributionControl()
        {
            InitializeComponent();
        }
    }
}
