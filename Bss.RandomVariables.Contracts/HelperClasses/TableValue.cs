﻿using System;

namespace Bss.RandomVariables.Contracts.HelperClasses
{
    /// <summary>
    /// Запись таблицы закона распределения
    /// </summary>
    public class TableValue
    {
        /// <summary>
        /// Значение случайной величины
        /// </summary>
        public double ValueX { get; set; }
        /// <summary>
        /// Вероятность возникновения значения
        /// </summary>
        public double ValueY { get; set; }

        /// <summary>
        /// Конструктор инициализации
        /// </summary>
        /// <param name="valueX">Значение случайной величины</param>
        /// <param name="valueY">Вероятоность значения</param>
        public TableValue(double valueX, double valueY)
        {
            ValueX = valueX;
            ValueY = valueY;
        }

        /// <summary>
        /// Переопредление операции равенства для использовании в тестах
        /// </summary>
        /// <param name="obj">Сравневыемый объект</param>
        /// <returns>Логический признак равенства</returns>
        public override bool Equals(object obj)
        {
            var comparingItem = (TableValue) obj;

            return Math.Abs(ValueX - comparingItem.ValueX) < Configuration.HighAccuracy &&
                   Math.Abs(ValueY - comparingItem.ValueY) < Configuration.HighAccuracy;
        }
    }
}