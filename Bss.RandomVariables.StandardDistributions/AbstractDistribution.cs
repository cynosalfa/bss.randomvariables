﻿using System;
using System.ComponentModel;

namespace Bss.RandomVariables.StandardDistributions
{
    public abstract class AbstractDistribution : INotifyPropertyChanged
    {
        private Func<double, double> _density;

        private double _rightEdge;
        private double _leftEdge;

        /// <summary>
        /// Правая граница
        /// </summary>
        public double RightEdge
        {
            get { return _rightEdge; }
            set
            {
                _rightEdge = value;
                OnPropertyChanged("RightEdge");
            }
        }

        /// <summary>
        /// Левая граница
        /// </summary>
        public double LeftEdge
        {
            get { return _leftEdge; }
            set
            {
                _leftEdge = value;
                OnPropertyChanged("LeftEdge");
            }
        }

        public Func<double, double> Density
        {
            get { return _density; }
            set
            {
                _density = value;
                OnPropertyChanged("Density");
            }
        }

        public abstract void BuildDensity();

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}