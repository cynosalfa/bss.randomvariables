﻿using System.Windows.Controls;

namespace Bss.RandomVariables.UI.Controls
{
    /// <summary>
    /// Контрол задания типа распределения
    /// </summary>
    public partial class DistributionControl : UserControl
    {
        /// <summary>
        /// Конструктор по умолчанию
        /// </summary>
        public DistributionControl()
        {
            InitializeComponent();
        }
    }
}
