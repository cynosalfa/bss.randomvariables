﻿using System;

namespace Bss.RandomVariables.StandardDistributions
{
    /// <summary>
    /// Нормальное распределение
    /// </summary>
    public sealed class Normal : AbstractDistribution
    {
        private double _average;
        private double _standardDeviation;

        public double Average
        {
            get { return _average; }
            set
            {
                _average = value;
                OnPropertyChanged("Average");
                BuildDensity();
            }
        }

        public double StandardDeviation
        {
            get { return _standardDeviation; }
            set
            {
                _standardDeviation = value;
                OnPropertyChanged("StandardDeviation");
                BuildDensity();
            }
        }

        public Normal() : this(0, 1)
        { }

        public Normal(double average, double standardDeviation)
        {
            Average = average;
            StandardDeviation = standardDeviation;

            BuildDensity();
        }

        public override void BuildDensity()
        {
            LeftEdge = Average - 3*StandardDeviation;
            RightEdge = Average + 3*StandardDeviation;

            Density = x =>
            {
                if (x < LeftEdge)
                    return 0;
                if (x > RightEdge)
                    return 0;
                return 1 / (StandardDeviation * Math.Pow(2 * Math.PI, 0.5)) *
                       Math.Exp(-Math.Pow(x - Average, 2) / (2 * Math.Pow(StandardDeviation, 2)));
            };
        }
    }
}