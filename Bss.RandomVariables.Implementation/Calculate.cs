﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Bss.RandomVariables.Contracts;
using Bss.RandomVariables.Contracts.Enums;
using Bss.RandomVariables.Contracts.HelperClasses;

namespace Bss.RandomVariables.Implementation
{
    public partial class RandomVariable
    {
        /// <summary>
        /// Метод, осуществляющий арифметическую операцию согласно переданному делегату
        /// </summary>
        /// <param name="operationDelegate">Делегат арифметической операции</param>
        /// <param name="operand">Правый операнд</param>
        /// <returns>Результат операции - новая случайная величина</returns>
        public AbstractRandomVariable Calculate(Func<double, double, double> operationDelegate,
            AbstractRandomVariable operand)
        {
            var valuesX = VariableDescriptor.Density.TableValues.Select(t => t.ValueX).ToList();
            var valuesY = operand.VariableDescriptor.Density.TableValues.Select(t => t.ValueX).ToList();
            valuesY.RemoveAll(y => Math.Abs(y) < Contracts.Configuration.HighAccuracy);
            var valuesZ = new ConcurrentBag<double>();

            Parallel.For(0, valuesX.Count,
                (i) => Parallel.For(0, valuesY.Count, (j) => valuesZ.Add(operationDelegate(valuesX[i], valuesY[j]))));

            var zMin = valuesZ.OrderBy(z => z).First();
            var zMax = valuesZ.OrderBy(z => z).Last();

            var h = (zMax - zMin)/valuesX.Count;
            var gistogram = new List<TableValue>();

            for (double z = zMin; z < zMax; z += h)
            {
                double localZ = z;
                var rate = valuesZ.Count(zx => zx >= localZ && zx < localZ + h)/(h * valuesZ.Count);

                gistogram.Add(new TableValue(localZ, rate));
            }

            var resultRandomVariable = new RandomVariable(new VariableDescriptor(gistogram, FunctionTypeEnum.Density));

            resultRandomVariable.CalculateParametersByTable();

            return resultRandomVariable;
        }
    }
}