﻿using System;
using System.Collections.Generic;
using Bss.RandomVariables.Contracts.Enums;
using Bss.RandomVariables.Contracts.HelperClasses;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Bss.RandomVariables.Contracts.Test
{
    [TestClass]
    public class AbstractRandomVariableTests
    {
        public class TestRandomVariable : AbstractRandomVariable
        {
            public TestRandomVariable(VariableDescriptor descriptor)
            {
                VariableDescriptor = descriptor;
            }

            protected override AbstractRandomVariable Add(AbstractRandomVariable operand)
            {
                throw new System.NotImplementedException();
            }

            protected override AbstractRandomVariable Subtract(AbstractRandomVariable operand)
            {
                throw new System.NotImplementedException();
            }

            protected override AbstractRandomVariable Multiply(AbstractRandomVariable operand)
            {
                throw new System.NotImplementedException();
            }

            protected override AbstractRandomVariable Divide(AbstractRandomVariable operand)
            {
                throw new System.NotImplementedException();
            }

            public override double Step
            {
                get { throw new NotImplementedException(); }
            }
        }

        [TestMethod]
        public void CalculateParametersByTableTest()
        {
            var table = new List<TableValue>
            {
                new TableValue(0, 0),
                new TableValue(1, 0.11),
                new TableValue(2, 0.21),
                new TableValue(3, 0.31),
                new TableValue(4, 0.51),
                new TableValue(5, 0.80),
                new TableValue(6, 0),
            };

            var testRandomVariableDescriptor = new VariableDescriptor(table, FunctionTypeEnum.Density);
            var testRandomVariable = new TestRandomVariable(testRandomVariableDescriptor);

            testRandomVariable.CalculateParametersByTable();

            const double expectingMathExpectation = 3;
            const double expectingVariance = 4;
            const double expectingStandardDeviation = 2;
            const double expectingSkew = 0;

            Assert.IsTrue(Math.Abs(expectingMathExpectation - testRandomVariable.MathExpectation) <
                          Configuration.DefaultAccuracy);
            Assert.IsTrue(Math.Abs(expectingVariance - testRandomVariable.Variance) < Configuration.DefaultAccuracy);
            Assert.IsTrue(Math.Abs(expectingStandardDeviation - testRandomVariable.StandardDeviation) <
                          Configuration.DefaultAccuracy);
            Assert.IsTrue(Math.Abs(expectingSkew - testRandomVariable.Skew) < Configuration.DefaultAccuracy);
        }
    }
}