using System;
using System.Collections.Generic;
using System.Linq;
using Bss.RandomVariables.Contracts.Enums;
using Bss.RandomVariables.Contracts.Extensions;

namespace Bss.RandomVariables.Contracts.HelperClasses
{
    /// <summary>
    /// �������� ��������� ��������
    /// </summary>
    public class VariableDescriptor
    {
        /// <summary>
        /// ��������� �������������
        /// </summary>
        public FunctionDescriptor Density { get; set; }

        /// <summary>
        /// ������� �������������
        /// </summary>
        public FunctionDescriptor Distribution { get; set; }

        /// <summary>
        /// ����������� �� ������� ��������
        /// </summary>
        /// <param name="tableValues">������� ��������</param>
        /// <param name="type">���</param>
        public VariableDescriptor(List<TableValue> tableValues, FunctionTypeEnum type)
        {
            switch (type)
            {
                case FunctionTypeEnum.Density:
                    Density = new FunctionDescriptor(type) { TableValues = tableValues };
                    break;
                default:
                    Distribution = new FunctionDescriptor(type) { TableValues = tableValues };
                    break;
            }
        }

        /// <summary>
        /// ����������� �� ��������
        /// </summary>
        /// <param name="functionDelegate">�������</param>
        /// <param name="interval">��������</param>
        /// <param name="step">��� ������������</param>
        public VariableDescriptor(Func<double, double> functionDelegate, Interval interval, double step)
        {
            DelegateScanner scanner;

            if (Math.Abs(functionDelegate(Double.PositiveInfinity) - 1) < Configuration.DefaultAccuracy &&
                Math.Abs(functionDelegate(Double.NegativeInfinity)) < Configuration.DefaultAccuracy)
            {
                scanner = new DelegateScanner(step, interval, functionDelegate, true);
                scanner.Scan();

                Distribution = new FunctionDescriptor(FunctionTypeEnum.Distribution)
                {
                    FunctionDelegate = functionDelegate,
                    TableValues = scanner.Table
                };
            }
            else if (Math.Abs(functionDelegate(Double.PositiveInfinity)) < Configuration.DefaultAccuracy &&
                     Math.Abs(functionDelegate(Double.NegativeInfinity)) < Configuration.DefaultAccuracy)
            {
                scanner = new DelegateScanner(step, interval, functionDelegate);
                scanner.Scan();

                Density = new FunctionDescriptor(FunctionTypeEnum.Density)
                {
                    FunctionDelegate = functionDelegate,
                    TableValues = scanner.Table
                };

                var selectionTable = new List<TableValue>();

                var d = Density.Convert();

                var orderredByY = d.TableValues.OrderBy(t => t.ValueY).ToList();
                var yMin = orderredByY.First().ValueY;
                var yMax = orderredByY.Last().ValueY;

                for (double y = yMin; y < yMax; y += step)
                {
                    selectionTable.Add(new TableValue(orderredByY.GetUndescribedXValue(y), y));
                }

                selectionTable.Add(new TableValue(orderredByY.GetUndescribedXValue(yMax), yMax));

                var newDensity = selectionTable.Select(t => t.ValueX).Select(x => new TableValue(x, functionDelegate(x))).ToList();
                Density.TableValues = newDensity;
            }
            else
            {
                throw new Exception(ErrorsEnum.VariableDescriptionError.GetDescriptionFromEnumValue());
            }
        }
    }
}