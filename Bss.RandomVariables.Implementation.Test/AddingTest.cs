﻿using System;
using System.IO;
using System.Linq;
using Bss.RandomVariables.Contracts;
using Bss.RandomVariables.Contracts.Calculations.Integration;
using Bss.RandomVariables.Contracts.Enums;
using Bss.RandomVariables.Contracts.HelperClasses;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Bss.RandomVariables.Implementation.Test
{
    [TestClass]
    public partial class AriphmeticalOperationsTests
    {
        [TestMethod]
        public void AddingTest()
        {
            AbstractRandomVariable leftOperand =
                new RandomVariable(new VariableDescriptor(x =>
                {
                    if (x < 0)
                        return 0;
                    if (x >= 0 && x <= 1)
                        return x;
                    return 1;
                }, 
                new Interval(0, 1), 0.05));

            AbstractRandomVariable rightOperand =
                new RandomVariable(new VariableDescriptor(x =>
                {
                    if (x < -1)
                        return 0;
                    if (x >= -1 && x <= 2)
                        return (x + 1)/3;
                    return 1;
                }, 
                new Interval(-1, 2), 0.05));

            var actual = leftOperand + rightOperand;
            var s =
                IntegrationMethods.Trapezoid.CreateIntegrator()
                    .Integrate(actual.VariableDescriptor.Density.TableValues);

            File.WriteAllLines("@test",
                actual.VariableDescriptor.Density.TableValues.Select(t => String.Format("({0};{1})", t.ValueX, t.ValueY)));
        }
    }
}