﻿using System;
using System.Collections.Generic;
using Bss.RandomVariables.Contracts.HelperClasses;

namespace Bss.RandomVariables.Contracts.Calculations.Integration
{
    /// <summary>
    /// Общая логика для методов интегрирования
    /// </summary>
    public abstract class AbstractIntegrator : IIntegration
    {
        public abstract string GetName();
        public abstract List<TableValue> TakeAntiderivative(List<TableValue> function);
        public abstract double Integrate(List<TableValue> function);

        public double Integrate(Func<double, double> function, Interval interval, double step)
        {
            var table = new DelegateScanner(step, interval, function);
            table.Scan();

            return Integrate(table.Table);
        }
    }
}