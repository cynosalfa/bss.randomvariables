﻿using System.Configuration;

namespace Bss.RandomVariables.Contracts
{
    /// <summary>
    /// Параметры конфигурации приложения
    /// </summary>
    public static class Configuration
    {
        private static double? _defaultAccuracy;
        private static double? _highAccuracy;
        private static double? _lowAccuracy;
        /// <summary>
        /// Точность по-умолчанию
        /// </summary>
        public static double DefaultAccuracy
        {
            get
            {
                if (_defaultAccuracy != null)
                {
                    return _defaultAccuracy.GetValueOrDefault();
                }

                double accuracy;
                if (double.TryParse(ConfigurationManager.AppSettings["defaultAccuracy"], out accuracy))
                {
                    _defaultAccuracy = accuracy;
                    return accuracy;
                }
                return 1e-6;
            }
        }

        /// <summary>
        /// Повышенная точность
        /// </summary>
        public static double HighAccuracy
        {
            get
            {
                if (_highAccuracy != null)
                {
                    return _highAccuracy.GetValueOrDefault();
                }

                double accuracy;
                if (double.TryParse(ConfigurationManager.AppSettings["highAccuracy"], out accuracy))
                {
                    _highAccuracy = accuracy;
                    return accuracy;
                }
                return 1e-9;
            }
        }

        /// <summary>
        /// Пониженная точность
        /// </summary>
        public static double LowAccuracy
        {
            get
            {
                if (_lowAccuracy != null)
                {
                    return _lowAccuracy.GetValueOrDefault();
                }

                double accuracy;
                if (double.TryParse(ConfigurationManager.AppSettings["lowAccuracy"], out accuracy))
                {
                    _lowAccuracy = accuracy;
                    return accuracy;
                }
                return 1e-3;
            }
        }

        /// <summary>
        /// Очистка значений конфигурации
        /// </summary>
        public static void ClearConfiguration()
        {
            _defaultAccuracy = null;
            _highAccuracy = null;
            _lowAccuracy = null;
        }
    }
}