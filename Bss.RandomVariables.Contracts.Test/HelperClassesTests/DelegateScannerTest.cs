﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Bss.RandomVariables.Contracts.HelperClasses;

namespace Bss.RandomVariables.Contracts.Test.HelperClassesTests
{
    [TestClass]
    public class DelegateScannerTest
    {
        [TestMethod]
        public void X2ScanningTest()
        {
            var x2Scanner = new DelegateScanner(0.1, new Interval(0, 1), x => Math.Pow(x, 2));
            x2Scanner.Scan();

            var existingValues = new List<TableValue>
            {
                new TableValue(0, 0),
                new TableValue(0.1, 0.01),
                new TableValue(0.201, 0.040401),
                new TableValue(0.302, 0.091204),
                new TableValue(0.403, 0.162409),
                new TableValue(0.504, 0.254016),
                new TableValue(0.595625, 0.354769140625),
                new TableValue(0.674412109375, 0.454831693271637),
                new TableValue(0.74455355834961, 0.554360001251066),
                new TableValue(0.809190868854523, 0.654789862237538),
                new TableValue(0.86877838947624, 0.75477589002093),
                new TableValue(0.924631698858459, 0.85494377853388),
                new TableValue(0.976994176404289, 0.954517620727896),
                new TableValue(1, 1)
            };

            Assert.AreEqual(existingValues.Count, x2Scanner.Table.Count);

            for (int i = 0; i < existingValues.Count; i++)
            {
                Assert.AreEqual(x2Scanner.Table[i], existingValues[i]);
            }
        }
    }
}