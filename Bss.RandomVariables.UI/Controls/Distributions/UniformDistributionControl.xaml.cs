﻿using System.Windows.Controls;

namespace Bss.RandomVariables.UI.Controls.Distributions
{
    /// <summary>
    /// Контрол задания равномерного распределения
    /// </summary>
    public partial class UniformDistributionControl : UserControl
    {
        /// <summary>
        /// Конструктор по умолчанию
        /// </summary>
        public UniformDistributionControl()
        {
            InitializeComponent();
        }
    }
}
