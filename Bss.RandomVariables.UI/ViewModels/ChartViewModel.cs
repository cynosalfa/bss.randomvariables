﻿using System.ComponentModel;
using System.Linq;
using Bss.RandomVariables.Contracts;
using OxyPlot;
using OxyPlot.Axes;
using OxyPlot.Series;

namespace Bss.RandomVariables.UI.ViewModels
{
    /// <summary>
    /// ViewModel для контрола визуализации графиков
    /// </summary>
    public class ChartViewModel : INotifyPropertyChanged
    {
        #region Fields
        /// <summary>
        /// Визуализируемая случайная величина
        /// </summary>
        public AbstractRandomVariable Variable { get; set; }

        private PlotModel _plotModel;

        /// <summary>
        /// Модель OxyPlot
        /// </summary>
        public PlotModel PlotModel
        {
            get { return _plotModel; }
            set
            {
                _plotModel = value;
                OnPropertyChanged("PlotModel");
            }
        }

        private string _title;

        /// <summary>
        /// Заголовок графикаы
        /// </summary>
        public string Title
        {
            get { return _title; }
            set
            {
                _title = value;
                OnPropertyChanged("Title");
            }
        }

        private double _mathExpectation;
        private double _variance;
        private double _standardDeviation;
        private double _skew;
        private double _square;

        /// <summary>
        /// Матожидание
        /// </summary>
        public double MathExpectation
        {
            get { return _mathExpectation; }
            set
            {
                _mathExpectation = value;
                OnPropertyChanged("MathExpectation");
            }
        }

        /// <summary>
        /// Дисперсия
        /// </summary>
        public double Variance
        {
            get { return _variance; }
            set
            {
                _variance = value;
                OnPropertyChanged("StandardDeviation");
            }
        }

        /// <summary>
        /// СКО
        /// </summary>
        public double StandardDeviation
        {
            get { return _standardDeviation; }
            set
            {
                _standardDeviation = value;
                OnPropertyChanged("StandardDeviation");
            }
        }

        /// <summary>
        /// Скос
        /// </summary>
        public double Skew
        {
            get { return _skew; }
            set
            {
                _skew = value;
                OnPropertyChanged("Skew");
            }
        }

        /// <summary>
        /// Площадь под графиком
        /// </summary>
        public double Square
        {
            get { return _square; }
            set
            {
                _square = value;
                OnPropertyChanged("Square");
            }
        }

        #endregion //Fields

        /// <summary>
        /// Конструктор по умолчанию
        /// </summary>
        public ChartViewModel() { }

        /// <summary>
        /// Конструктор инициализации
        /// </summary>
        /// <param name="variable">Случайная величина для визуализации</param>
        public ChartViewModel(AbstractRandomVariable variable)
        {
            variable.CalculateParametersByTable();
            Variable = variable;

            MathExpectation = variable.MathExpectation <= Configuration.HighAccuracy ? 0 : variable.MathExpectation;
            Variance = variable.Variance <= Configuration.HighAccuracy ? 0 : variable.Variance;
            StandardDeviation = variable.StandardDeviation <= Configuration.HighAccuracy ? 0 : variable.StandardDeviation;
            Skew = variable.Skew <= Configuration.HighAccuracy ? 0 : variable.Skew;
            Square = variable.VariableDescriptor.Density.Square;

            var lineSeries = new LineSeries();
            lineSeries.Points.AddRange(variable.VariableDescriptor.Density.TableValues.Select(t => new DataPoint(t.ValueX, t.ValueY)).ToList());
            lineSeries.Color = OxyColor.FromRgb(0, 0, 255);

            var tempModel = new PlotModel();
            tempModel.Series.Add(lineSeries);

            tempModel.Axes.Add(new LinearAxis(AxisPosition.Left) { MajorGridlineStyle = LineStyle.Solid });
            tempModel.Axes.Add(new LinearAxis(AxisPosition.Bottom) { MajorGridlineStyle = LineStyle.Solid });

            PlotModel = tempModel;
        }

        /// <summary>
        /// Событие изменения свойства
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Взвод события изменения свойства
        /// </summary>
        /// <param name="propertyName"></param>
        protected virtual void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}