﻿using System.ComponentModel;
using System.Windows.Controls;
using System.Windows.Input;
using Bss.RandomVariables.Contracts;
using Bss.RandomVariables.Contracts.HelperClasses;
using Bss.RandomVariables.Implementation;
using Bss.RandomVariables.StandardDistributions;
using Bss.RandomVariables.UI.Controls;
using Bss.RandomVariables.UI.Utils;
using Configuration = Bss.RandomVariables.Implementation.Configuration;

namespace Bss.RandomVariables.UI.ViewModels
{
    /// <summary>
    /// ViewModel главно страницы
    /// </summary>
    public class MainViewModel : INotifyPropertyChanged
    {
        #region Fields
        private ChartViewModel _variable1;
        private ChartViewModel _variable2;
        private ChartViewModel _sum;
        private ChartViewModel _subtract;
        private ChartViewModel _multiply;
        private ChartViewModel _divide;

        public ChartViewModel Variable1
        {
            get { return _variable1; }
            set
            {
                _variable1 = value;
                OnPropertyChanged("Variable1");
            }
        }

        public ChartViewModel Variable2
        {
            get { return _variable2; }
            set
            {
                _variable2 = value;
                OnPropertyChanged("Variable2");
            }
        }

        public ChartViewModel Sum
        {
            get { return _sum; }
            set
            {
                _sum = value;
                OnPropertyChanged("Sum");
            }
        }

        public ChartViewModel Subtract
        {
            get { return _subtract; }
            set
            {
                _subtract = value;
                OnPropertyChanged("Subtract");
            }
        }

        public ChartViewModel Multiply
        {
            get { return _multiply; }
            set
            {
                _multiply = value;
                OnPropertyChanged("Multiply");
            }
        }

        public ChartViewModel Divide
        {
            get { return _divide; }
            set
            {
                _divide = value;
                OnPropertyChanged("Divide");
            }
        }

        public DistributionViewModel LeftDistributionModel { get; set; }
        public DistributionViewModel RightDistributionModel { get; set; }

        #endregion //Fields

        /// <summary>
        /// Команда построения левого операнда
        /// </summary>
        public ICommand BuildLeftOperandCommand { get; set; }
        /// <summary>
        /// Команда построения правго операнда
        /// </summary>
        public ICommand BuildRightOperandCommand { get; set; }

        /// <summary>
        /// Конструктор по умолчанию
        /// </summary>
        public MainViewModel()
        {
            BuildLeftOperandCommand = new DelegateCommand(BuildLeftOperand);
            BuildRightOperandCommand = new DelegateCommand(BuildRightOperand);

            LeftDistributionModel = new DistributionViewModel();
            RightDistributionModel = new DistributionViewModel();
        }

        private void BuildLeftOperand(object obj)
        {
            var tabItemHeader = ((TabItem)((DistributionControl)obj).Tabs.SelectedItem).Header.ToString();
            var distribution = LeftDistributionModel.GetDistribution(tabItemHeader);

            Variable1 = SetVariable(obj, distribution);

            if (Variable2 != null)
            {
                Calc(Variable1.Variable, Variable2.Variable);
            }
        }

        private void BuildRightOperand(object obj)
        {
            var tabItemHeader = ((TabItem)((DistributionControl)obj).Tabs.SelectedItem).Header.ToString();
            var distribution = RightDistributionModel.GetDistribution(tabItemHeader);

            Variable2 = SetVariable(obj, distribution);

            if (Variable1 != null)
            {
                Calc(Variable1.Variable, Variable2.Variable);
            }
        }

        /// <summary>
        /// Устанавливает значение перменной
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="distribution">Распределение</param>
        /// <returns></returns>
        public ChartViewModel SetVariable(object obj, AbstractDistribution distribution)
        {
            var variable = new ChartViewModel();
            
            distribution.BuildDensity();

            if (distribution is Uniform)
            {
                variable = new ChartViewModel(
                    new RandomVariable(new VariableDescriptor(distribution.Density,
                        new Interval(distribution.LeftEdge, distribution.RightEdge),
                        Configuration.Step)));
            }

            if (distribution is Normal)
            {
                variable = new ChartViewModel(
                    new RandomVariable(new VariableDescriptor(distribution.Density, new Interval(distribution.LeftEdge, distribution.RightEdge),
                        Configuration.Step)));
            }

            if (distribution is Exponential)
            {
                variable = new ChartViewModel(
                    new RandomVariable(new VariableDescriptor(distribution.Density, new Interval(0, distribution.RightEdge),
                        Configuration.Step)));
            }

            return variable;
        }

        /// <summary>
        /// Выполняет арифметические операции
        /// </summary>
        /// <param name="leftOperand">Левый операнд</param>
        /// <param name="rightOperand">Правый операнд</param>
        public void Calc(AbstractRandomVariable leftOperand, AbstractRandomVariable rightOperand)
        {
            Sum = new ChartViewModel(leftOperand + rightOperand);
            Subtract = new ChartViewModel(leftOperand - rightOperand);
            Multiply = new ChartViewModel(leftOperand * rightOperand);
            Divide = new ChartViewModel(leftOperand / rightOperand);
        }

        /// <summary>
        /// Изменение свойства
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Взводит событие измнения свойства
        /// </summary>
        /// <param name="propertyName"></param>
        protected virtual void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}