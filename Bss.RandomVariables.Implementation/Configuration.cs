﻿using System;
using System.Configuration;

namespace Bss.RandomVariables.Implementation
{
    /// <summary>
    /// Сборщик конфигурации
    /// </summary>
    public static class Configuration
    {
        private static double? _step;
        
        /// <summary>
        /// Шаг
        /// </summary>
        public static double Step
        {
            get
            {
                if (_step != null)
                {
                    return _step.GetValueOrDefault();
                }

                double step;
                if (double.TryParse(ConfigurationManager.AppSettings["step"], out step))
                {
                    _step = step;
                    return step;
                }
                return 1e-2;
            }
        }

        /// <summary>
        /// Очистка значений конфигурации
        /// </summary>
        public static void ClearConfiguration()
        {
            _step = null;
        }
    }
}