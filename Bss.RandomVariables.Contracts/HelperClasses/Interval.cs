﻿namespace Bss.RandomVariables.Contracts.HelperClasses
{
    /// <summary>
    /// Интервал
    /// </summary>
    public class Interval
    {
        /// <summary>
        /// Левая граница
        /// </summary>
        public double LeftEdge { get; private set; }
        /// <summary>
        /// Правая граница
        /// </summary>
        public double RightEdge { get; private set; }
        /// <summary>
        /// Длина интервала
        /// </summary>
        public double Lenght
        {
            get { return RightEdge - LeftEdge; }
        }

        /// <summary>
        /// Конструктор инициализации
        /// </summary>
        /// <param name="leftEdge">Левая граница интервала</param>
        /// <param name="rightEdge">Правая граница интервала</param>
        public Interval(double leftEdge, double rightEdge)
        {
            LeftEdge = leftEdge;
            RightEdge = rightEdge;
        }
    }
}