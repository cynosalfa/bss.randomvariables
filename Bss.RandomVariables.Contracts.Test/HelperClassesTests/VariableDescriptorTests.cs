﻿using System;
using Bss.RandomVariables.Contracts.Enums;
using Bss.RandomVariables.Contracts.HelperClasses;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Bss.RandomVariables.Contracts.Test.HelperClassesTests
{
    [TestClass]
    public class VariableDescriptorTests
    {
        [TestMethod]
        public void FunctionTypeDetectingTest()
        {
            var distributionDelegate = new Func<double, double>(d =>
            {
                if (double.IsPositiveInfinity(d))
                {
                    return 1;
                }
                if (double.IsNegativeInfinity(d))
                {
                    return 0;
                }
                return Math.Pow(d, 2);
            });

            var densityDelegate = new Func<double, double>(d =>
            {
                if (double.IsPositiveInfinity(d))
                {
                    return 0;
                }
                if (double.IsNegativeInfinity(d))
                {
                    return 0;
                }
                return Math.Pow(d, 2);
            });

            var distributionDescriptor = new VariableDescriptor(distributionDelegate, new Interval(0,1), 0.1);
            var densityDescriptor = new VariableDescriptor(densityDelegate, new Interval(0, 1), 0.1);

            Assert.AreEqual(distributionDescriptor.Distribution.FunctionType, FunctionTypeEnum.Distribution);
            Assert.AreEqual(densityDescriptor.Density.FunctionType, FunctionTypeEnum.Density);
        }
    }
}
