﻿using System;
using System.Linq;
using Bss.RandomVariables.Contracts.HelperClasses;

namespace Bss.RandomVariables.Contracts
{
    /// <summary>
    /// Описывает базовые параметры случайной величины и операции над ней
    /// </summary>
    public abstract class AbstractRandomVariable
    {
        /// <summary>
        /// Дескриптор случайной величины
        /// </summary>
        public VariableDescriptor VariableDescriptor { get; protected set; }

        #region variable parameters

        /// <summary>
        /// Матожидание
        /// </summary>
        public double MathExpectation { get; protected set; }
        /// <summary>
        /// Дисперсия
        /// </summary>
        public double Variance { get; protected set; }
        /// <summary>
        /// Среднеквадратичное отклонение
        /// </summary>
        public double StandardDeviation { get; protected set; }
        /// <summary>
        /// Коэффициент скоса
        /// </summary>
        public double Skew { get; protected set; }

        #endregion //variable parameters

        /// <summary>
        /// Вычисляет основные параметры случайно величины по её дескриптору
        /// </summary>
        public void CalculateParametersByTable()
        {
            if (VariableDescriptor.Density == null)
            {
                VariableDescriptor.Density = VariableDescriptor.Distribution.Convert();
            }

            var tableValues = VariableDescriptor.Density.TableValues;

            MathExpectation = tableValues.Sum(val => val.ValueX) / tableValues.Count;
            Variance = tableValues.Sum(val => Math.Pow(val.ValueX - MathExpectation, 2)) / tableValues.Count;
            StandardDeviation = Math.Pow(Variance, 0.5);
            Skew = tableValues.Sum(val => Math.Pow(val.ValueX - MathExpectation, 3)) / tableValues.Count /
                   Math.Pow(StandardDeviation, 3);
        }

        /// <summary>
        /// Операция сложения случайных величин
        /// </summary>
        /// <param name="operand">Второй операнд</param>
        /// <returns>Результат сложения - новая случайная величина</returns>
        protected abstract AbstractRandomVariable Add(AbstractRandomVariable operand);

        /// <summary>
        /// Оператор сложения
        /// </summary>
        /// <param name="leftOperand">Первый операнд</param>
        /// <param name="rightOperand">Второй операнд</param>
        /// <returns>Результат сложения - новая случайная величина</returns>
        public static AbstractRandomVariable operator +(AbstractRandomVariable leftOperand, AbstractRandomVariable rightOperand)
        {
            return leftOperand.Add(rightOperand);
        }

        /// <summary>
        /// Операция вычитания случайных величин
        /// </summary>
        /// <param name="operand">Второй операнд</param>
        /// <returns>Результат вычитания - новая случайная величина</returns>
        protected abstract AbstractRandomVariable Subtract(AbstractRandomVariable operand);

        /// <summary>
        /// Оператор вычитания
        /// </summary>
        /// <param name="leftOperand">Первый операнд</param>
        /// <param name="rightOperand">Второй операнд</param>
        /// <returns>Результат вычитания - новая случайная величина</returns>
        public static AbstractRandomVariable operator -(AbstractRandomVariable leftOperand, AbstractRandomVariable rightOperand)
        {
            return leftOperand.Subtract(rightOperand);
        }

        /// <summary>
        /// Операция умножения случайных величин
        /// </summary>
        /// <param name="operand">Второй операнд</param>
        /// <returns>Результат умножения - новая случайная величина</returns>
        protected abstract AbstractRandomVariable Multiply(AbstractRandomVariable operand);

        /// <summary>
        /// Оператор умножения
        /// </summary>
        /// <param name="leftOperand">Первый операнд</param>
        /// <param name="rightOperand">Второй операнд</param>
        /// <returns>Результат умножения - новая случайная величина</returns>
        public static AbstractRandomVariable operator *(AbstractRandomVariable leftOperand, AbstractRandomVariable rightOperand)
        {
            return leftOperand.Multiply(rightOperand);
        }

        /// <summary>
        /// Операция деления случайных величин
        /// </summary>
        /// <param name="operand">Второй операнд</param>
        /// <returns>Результат деления - новая случайная величина</returns>
        protected abstract AbstractRandomVariable Divide(AbstractRandomVariable operand);

        /// <summary>
        /// Оператор деления
        /// </summary>
        /// <param name="leftOperand">Первый операнд</param>
        /// <param name="rightOperand">Второй операнд</param>
        /// <returns>Результат деления - новая случайная величина</returns>
        public static AbstractRandomVariable operator /(AbstractRandomVariable leftOperand, AbstractRandomVariable rightOperand)
        {
            return leftOperand.Divide(rightOperand);
        }

        /// <summary>
        /// Шаг
        /// </summary>
        public abstract double Step { get; }
    }
}