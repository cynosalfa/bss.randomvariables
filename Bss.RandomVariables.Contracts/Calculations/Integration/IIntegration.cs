﻿using System;
using System.Collections.Generic;
using Bss.RandomVariables.Contracts.HelperClasses;

namespace Bss.RandomVariables.Contracts.Calculations.Integration
{
    /// <summary>
    /// Интерфейс для операций интегрирования
    /// </summary>
    public interface IIntegration
    {
        /// <summary>
        /// Позволяет получить название метода интегрирования
        /// </summary>
        /// <returns>Название метода интегрирования</returns>
        string GetName();

        /// <summary>
        /// Вычисляет интеграл делегата в интервале
        /// </summary>
        /// <param name="function">Делегат</param>
        /// <param name="interval">Интервал интегрирования</param>
        /// <param name="step">Шаг</param>
        /// <returns>Значение интеграла</returns>
        double Integrate(Func<double, double> function, Interval interval, double step);

        /// <summary>
        /// Вычисляет интеграл таблично заданной функции в интервале
        /// </summary>
        /// <param name="function">Таблица значений</param>
        /// <returns>Значение интеграла</returns>
        double Integrate(List<TableValue> function);

        /// <summary>
        /// Вычисляет интеграл таблично заданной функции в интервале
        /// </summary>
        /// <param name="function">Таблица значений</param>
        /// <returns>Значение интеграла</returns>
        List<TableValue> TakeAntiderivative(List<TableValue> function);
    }
}