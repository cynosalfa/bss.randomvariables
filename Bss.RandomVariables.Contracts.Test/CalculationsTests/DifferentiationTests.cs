﻿using System;
using System.Collections.Generic;
using Bss.RandomVariables.Contracts.Calculations;
using Bss.RandomVariables.Contracts.HelperClasses;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Bss.RandomVariables.Contracts.Test.CalculationsTests
{
    [TestClass]
    public class DifferentiationTests
    {
        [TestMethod]
        public void TakeDerivativeTest()
        {
            var x2Scanner = new DelegateScanner(0.1, new Interval(0, 1), x => Math.Pow(x, 3));
            x2Scanner.Scan();
           
            var actual = x2Scanner.Table.TakeDerivative();
            var expected = new List<TableValue>
            {
                new TableValue(0, 0.01),
                new TableValue(0.1, 0.070501),
                new TableValue(0.201, 0.192307),
                new TableValue(0.302, 0.375319),
                new TableValue(0.403, 0.619537),
                new TableValue(0.504, 0.924961),
                new TableValue(0.605, 1.25103328515625),
                new TableValue(0.6856875, 1.54690095596409),
                new TableValue(0.7499853515625, 1.81469324026322),
                new TableValue(0.805191513061523, 2.06399387643282),
                new TableValue(0.853486982345581, 2.29826854087529),
                new TableValue(0.896864652916789, 2.52133185264376),
                new TableValue(0.936507094023633, 2.73513752458501),
                new TableValue(0.973049117842989, 2.91987370357801)
            };

            Assert.AreEqual(expected.Count, actual.Count);

            for (int i = 0; i < expected.Count; i++)
            {
                Assert.AreEqual(expected[i], actual[i]);
            }
        }
    }
}