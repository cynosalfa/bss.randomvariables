﻿namespace Bss.RandomVariables.StandardDistributions
{
    /// <summary>
    /// Равномерное распределение
    /// </summary>
    public sealed class Uniform : AbstractDistribution
    {
        public Uniform() : this(0, 1)
        { }

        public Uniform(double leftEdge, double rightEdge)
        {
            LeftEdge = leftEdge;
            RightEdge = rightEdge;

            BuildDensity();
        }

        public override void BuildDensity()
        {
            Density = x =>
            {
                if (x < LeftEdge)
                    return 0;
                if (x >= LeftEdge && x <= RightEdge)
                    return 1/(RightEdge - LeftEdge);

                return 0;
            };
        }
    }
}