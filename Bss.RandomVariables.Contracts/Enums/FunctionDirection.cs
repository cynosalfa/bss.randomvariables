﻿namespace Bss.RandomVariables.Contracts.Enums
{
    /// <summary>
    /// Характер изменения значений функции
    /// </summary>
    public enum FunctionDirection
    {
        /// <summary>
        /// Увеличение
        /// </summary>
        Increase,
        /// <summary>
        /// Уменьшение
        /// </summary>
        Decrease
    }
}