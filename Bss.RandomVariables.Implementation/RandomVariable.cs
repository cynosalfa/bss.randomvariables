﻿using Bss.RandomVariables.Contracts;
using Bss.RandomVariables.Contracts.HelperClasses;

namespace Bss.RandomVariables.Implementation
{
    /// <summary>
    /// Реализация случайной величины
    /// </summary>
    public partial class RandomVariable : AbstractRandomVariable
    {
        /// <summary>
        /// Конструктор инициализации
        /// </summary>
        /// <param name="descriptor"></param>
        public RandomVariable(VariableDescriptor descriptor)
        {
            VariableDescriptor = descriptor;
        }

        protected override AbstractRandomVariable Add(AbstractRandomVariable operand)
        {
            return Calculate((x, y) => x + y, operand);
        }

        protected override AbstractRandomVariable Subtract(AbstractRandomVariable operand)
        {
            return Calculate((x, y) => x - y, operand);
        }

        protected override AbstractRandomVariable Multiply(AbstractRandomVariable operand)
        {
            return Calculate((x, y) => x * y, operand);
        }

        protected override AbstractRandomVariable Divide(AbstractRandomVariable operand)
        {
            return Calculate((x, y) => x / y, operand);
        }

        public override double Step { get { return Configuration.Step; } }
    }
}