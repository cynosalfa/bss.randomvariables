﻿using System.Collections.Generic;
using System.Linq;
using Bss.RandomVariables.Contracts.Enums;
using Bss.RandomVariables.Contracts.HelperClasses;

namespace Bss.RandomVariables.Contracts.Calculations.Integration
{
    /// <summary>
    /// Интегрирование методом трапеций
    /// </summary>
    public class Trapezoid : AbstractIntegrator
    {
        public override string GetName()
        {
            return IntegrationMethods.Trapezoid.GetDescriptionFromEnumValue();
        }

        public override double Integrate(List<TableValue> function)
        {
            double square = 0;

            for (int i = 1; i < function.Count; i++)
            {
                square += MethodImpemlentation(function, i);
            }

            return square;
        }

        public override List<TableValue> TakeAntiderivative(List<TableValue> function)
        {
            var table = new List<TableValue>();
            double square = 0;

            table.Add(new TableValue(function[0].ValueX, 0));

            for (int i = 1; i < function.Count; i++)
            {
                square += MethodImpemlentation(function, i);
                table.Add(new TableValue(function[i].ValueX, square));
            }

            return table;
        }

        private double MethodImpemlentation(List<TableValue> function, int i)
        {
            return (function[i - 1].ValueY + function[i].ValueY) * (function[i].ValueX - function[i - 1].ValueX) / 2;
        }
    }
}