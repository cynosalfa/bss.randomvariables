﻿using System;
using Bss.RandomVariables.Contracts.Enums;
using Bss.RandomVariables.StandardDistributions;

namespace Bss.RandomVariables.UI.ViewModels
{
    /// <summary>
    /// ViewModel контрола выбора распределения
    /// </summary>
    public class DistributionViewModel
    {
        /// <summary>
        /// Модель нормального распределения
        /// </summary>
        public Normal NormalModel { get; set; }
        /// <summary>
        /// Модель экспоненциального распределения
        /// </summary>
        public Exponential ExponentialModel { get; set; }
        /// <summary>
        /// Модель равномерного распределения
        /// </summary>
        public Uniform UniformModel { get; set; }

        /// <summary>
        /// Заголовок нормального распределения
        /// </summary>
        public string NormalHeader { get { return "Нормальное"; } }
        /// <summary>
        /// Заголовок экспоненциального распределения
        /// </summary>
        public string ExponentialHeader { get { return "Экспоненциальное"; } }
        /// <summary>
        /// Заголовок равномерного распределения
        /// </summary>
        public string UniformHeader { get { return "Равномерное"; } }

        /// <summary>
        /// Конструктор по умолчанию
        /// </summary>
        public DistributionViewModel()
        {
            NormalModel = new Normal();
            ExponentialModel = new Exponential();
            UniformModel = new Uniform();
        }

        /// <summary>
        /// Выбор модели распредления по заголовку
        /// </summary>
        /// <param name="header">Заголовок</param>
        /// <returns>Модель распределения</returns>
        public AbstractDistribution GetDistribution(string header)
        {
            if (header == NormalHeader)
                return NormalModel;
            if (header == ExponentialHeader)
                return ExponentialModel;
            if (header == UniformHeader)
                return UniformModel;

            throw new Exception(ErrorsEnum.UnexpectedEnumError.GetDescriptionFromEnumValue());
        }
    }
}