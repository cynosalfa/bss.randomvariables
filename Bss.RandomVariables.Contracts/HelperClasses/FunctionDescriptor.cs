using System;
using System.Collections.Generic;
using System.Linq;
using Bss.RandomVariables.Contracts.Calculations;
using Bss.RandomVariables.Contracts.Calculations.Integration;
using Bss.RandomVariables.Contracts.Enums;

namespace Bss.RandomVariables.Contracts.HelperClasses
{
    /// <summary>
    /// �������� ������������
    /// </summary>
    public class FunctionDescriptor
    {
        /// <summary>
        /// ����� ��������������
        /// </summary>
        public IntegrationMethods IntegrationMethod { get; set; }

        /// <summary>
        /// ����������� �������������
        /// </summary>
        /// <param name="type">��� �������</param>
        public FunctionDescriptor(FunctionTypeEnum type)
        {
            FunctionType = type;
            IntegrationMethod = IntegrationMethods.Simpson;
        }

        /// <summary>
        /// ������� �������������
        /// </summary>
        public Func<double, double> FunctionDelegate { get; set; }

        /// <summary>
        /// ��� ��������
        /// </summary>
        public FunctionTypeEnum FunctionType { get; private set; }

        /// <summary>
        /// ��������� ����� �������������
        /// </summary>
        public List<TableValue> TableValues { get; set; }

        private double? _square;

        /// <summary>
        /// ������� ��� ��������
        /// </summary>
        public double Square
        {
            get
            {
                if (_square == null && TableValues != null)
                {
                    try
                    {
                        _square = IntegrationMethod.CreateIntegrator().Integrate(TableValues);
                    }
                    catch
                    {
                        _square = null;
                        return 0d;
                    }
                }
                else if (_square == null)
                {
                    return 0;
                }

                return _square.GetValueOrDefault();
            }
        }

        /// <summary>
        /// ������������ ��������� � ������������� � ��������
        /// </summary>
        /// <returns></returns>
        public FunctionDescriptor Convert()
        {
            FunctionDescriptor descriptor;

            switch (FunctionType)
            {
                case FunctionTypeEnum.Density:
                    descriptor = new FunctionDescriptor(FunctionTypeEnum.Distribution);
                    var integrator = IntegrationMethod.CreateIntegrator();
                    descriptor.TableValues = integrator.TakeAntiderivative(TableValues);
                    break;
                case FunctionTypeEnum.Distribution:
                    descriptor = new FunctionDescriptor(FunctionTypeEnum.Density)
                    {
                        TableValues = TableValues.TakeDerivative()
                    };
                    descriptor.TableValues.Add(new TableValue(TableValues.Last().ValueX, 1));
                    break;
                default:
                    throw new Exception(ErrorsEnum.UnexpectedEnumError.GetDescriptionFromEnumValue());
            }

            return descriptor;
        }
    }
}