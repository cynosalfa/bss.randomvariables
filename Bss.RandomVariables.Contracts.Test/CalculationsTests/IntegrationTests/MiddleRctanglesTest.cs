﻿using System;
using System.Collections.Generic;
using Bss.RandomVariables.Contracts.Calculations.Integration;
using Bss.RandomVariables.Contracts.Enums;
using Bss.RandomVariables.Contracts.HelperClasses;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Bss.RandomVariables.Contracts.Test.CalculationsTests.IntegrationTests
{
    public partial class IntegrationTests
    {
        [TestMethod]
        public void MiddleRectanglesAntiderivativeTest()
        {
            var integrator = IntegrationMethods.Simpson.CreateIntegrator();
            var scanner = new DelegateScanner(0.1, new Interval(0, 1), x => 3*Math.Pow(x, 2));
            scanner.Scan();

            var actual = integrator.TakeAntiderivative(scanner.Table);
            var expected = new List<TableValue>
            {
                new TableValue(0, 0),
                new TableValue(0.1, 0.0015),
                new TableValue(0.201, 0.0091357515),
                new TableValue(0.27153125, 0.0212103720901337),
                new TableValue(0.327091064453125, 0.0362713419255681),
                new TableValue(0.374788890838623, 0.053975960196447),
                new TableValue(0.416884269326925, 0.0738192505711756),
                new TableValue(0.455022155492567, 0.095605765329429),
                new TableValue(0.490174015569224, 0.119191735125677),
                new TableValue(0.5231229903837, 0.144591847516919),
                new TableValue(0.554012654272272, 0.171493085149322),
                new TableValue(0.583329881960051, 0.199954415677864),
                new TableValue(0.611271670707325, 0.229877003694282),
                new TableValue(0.63790368810707, 0.26105940103849),
                new TableValue(0.663493232040198, 0.293576458696719),
                new TableValue(0.688182114057681, 0.327418171843934),
                new TableValue(0.712003027566738, 0.36245431445563),
                new TableValue(0.735078583012147, 0.398704471806294),
                new TableValue(0.757433027349887, 0.436060267752737),
                new TableValue(0.779088895302073, 0.474413382619164),
                new TableValue(0.800235475251752, 0.513979362922652),
                new TableValue(0.820804665695286, 0.554524233654677),
                new TableValue(0.840890948072415, 0.596127480602345),
                new TableValue(0.860506458206329, 0.638719685749018),
                new TableValue(0.87966222982148, 0.682230402401773),
                new TableValue(0.898443106808082, 0.726769411477116),
                new TableValue(0.916784547677996, 0.772101032468632),
                new TableValue(0.93476703105979, 0.818341639148167),
                new TableValue(0.952433061500726, 0.865534270232646),
                new TableValue(0.96975439995899, 0.913537389148386),
                new TableValue(0.986804409378017, 0.962493248418223),
                new TableValue(1, 1.00156109594443)
            };

            Assert.AreEqual(expected.Count, actual.Count);

            for (int i = 0; i < expected.Count; i++)
            {
                Assert.AreEqual(expected[i], actual[i]);
            }
        }
    }
}