﻿using System.Collections.Generic;
using System.Linq;
using Bss.RandomVariables.Contracts.Enums;
using Bss.RandomVariables.Contracts.HelperClasses;

namespace Bss.RandomVariables.Contracts.Calculations.Integration
{
    /// <summary>
    /// Метод Симпсона
    /// </summary>
    public class Simpson : AbstractIntegrator
    {
        public override string GetName()
        {
            return IntegrationMethods.Simpson.GetDescriptionFromEnumValue();
        }

        public override double Integrate(List<TableValue> function)
        {
            double square = 0;

            for (int i = 1; i < function.Count; i++)
            {
                square += MethodImplementation(function, i);
            }

            return square;
        }

        public override List<TableValue> TakeAntiderivative(List<TableValue> function)
        {
            var table = new List<TableValue>();
            double square = 0;

            table.Add(new TableValue(function[0].ValueX, 0));

            for (int i = 1; i < function.Count; i++)
            {
                square += MethodImplementation(function, i);
                table.Add(new TableValue(function[i].ValueX, square));
            }

            return table;
        }

        private double MethodImplementation(List<TableValue> function, int i)
        {
            var middleX = (function[i].ValueX + function[i - 1].ValueX) / 2;
            var linearCoefficients = FunctionDetector.Linear(function[i - 1], function[i]);
            var middleY = linearCoefficients.Item1 * middleX + linearCoefficients.Item2;

            return ((function[i].ValueX - function[i - 1].ValueX) / 6) *
                          (function[i - 1].ValueY + 4 * middleY + function[i].ValueY);
        }
    }
}