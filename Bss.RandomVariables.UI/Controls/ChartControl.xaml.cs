﻿using System.Windows.Controls;

namespace Bss.RandomVariables.UI.Controls
{
    /// <summary>
    /// Контрол визуализации графиков
    /// </summary>
    public partial class ChartControl : UserControl
    {
        /// <summary>
        /// Конструктор по умолчанию
        /// </summary>
        public ChartControl()
        {
            InitializeComponent();
        }
    }
}
