﻿using System.ComponentModel;

namespace Bss.RandomVariables.Contracts.Enums
{
    /// <summary>
    /// Методы интегрирования
    /// </summary>
    public enum IntegrationMethods
    {
        /// <summary>
        /// Метод левых прямоугольников
        /// </summary>
        [Description("Метод левых прямоугольников")]
        LeftRectangles = 1,
        /// <summary>
        /// Метод правх прямоугольников
        /// </summary>
        [Description("Метод правых прямоугольников")]
        RightReactangles = 2,
        /// <summary>
        /// Метод средних прямоугольников
        /// </summary>
        [Description("Метод средних прямоугольников")]
        MiddleRectangles = 3,
        /// <summary>
        /// Метод трапеций
        /// </summary>
        [Description("Метод трапеций")]
        Trapezoid = 0, //default
        /// <summary>
        /// Метод Симпсона
        /// </summary>
        [Description("Метод Симпсона")]
        Simpson = 4
    }
}