﻿using System;

namespace Bss.RandomVariables.StandardDistributions
{
    /// <summary>
    /// Экспоненциальное распределение
    /// </summary>
    public sealed class Exponential : AbstractDistribution
    {
        private double _lambda;

        public Exponential() : this(2, 4)
        { }

        public Exponential(double lambda, double rightEdge)
        {
            Lambda = lambda;
            RightEdge = rightEdge;
            BuildDensity();
        }

        public double Lambda
        {
            get { return _lambda; }
            set
            {
                _lambda = value;
                OnPropertyChanged("Lambda");
            }
        }

        public override void BuildDensity()
        {
            Density = x =>
            {
                if (x < 0)
                    return 0;
                if (x > RightEdge)
                    return 0;

                return _lambda * Math.Exp(-_lambda * x);
            };
        }
    }
}