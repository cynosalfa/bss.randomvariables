﻿using System.Collections.Generic;
using System.Linq;
using Bss.RandomVariables.Contracts.Enums;
using Bss.RandomVariables.Contracts.HelperClasses;

namespace Bss.RandomVariables.Contracts.Calculations.Integration
{
    /// <summary>
    /// Интегрирование методом левых прямоугольников
    /// </summary>
    public class LeftRectangles : AbstractIntegrator
    {
        public override string GetName()
        {
            return IntegrationMethods.LeftRectangles.GetDescriptionFromEnumValue();
        }

        public override double Integrate(List<TableValue> function)
        {
            double square = 0;

            for (int i = 0; i < function.Count - 1; i++)
            {
                square += MethodImplementation(function, i);
            }

            return square;
        }

        public override List<TableValue> TakeAntiderivative(List<TableValue> function)
        {
            var table = new List<TableValue>();
            double square = 0;
            
            for (int i = 0; i < function.Count - 1; i++)
            {
                square += MethodImplementation(function, i);
                table.Add(new TableValue(function[i].ValueX, square));
            }

            table.Add(new TableValue(function.Last().ValueX, 1));

            return table;
        }

        private double MethodImplementation(List<TableValue> function, int i)
        {
            return function[i].ValueY * (function[i + 1].ValueX - function[i].ValueX);
        }
    }
}