﻿using System;
using Bss.RandomVariables.Contracts.Calculations;
using Bss.RandomVariables.Contracts.HelperClasses;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Bss.RandomVariables.Contracts.Test.CalculationsTests
{
    [TestClass]
    public class FunctionDetectorTest
    {
        [TestMethod]
        public void LinearTest()
        {
            var actual = FunctionDetector.Linear(new TableValue(1, 0), new TableValue(2, 1));
            var expected = new Tuple<double, double>(1,-1);

           Assert.AreEqual(actual.Item1, expected.Item1);
           Assert.AreEqual(actual.Item2, expected.Item2);
        }
    }
}
