﻿using System;
using Bss.RandomVariables.Contracts.HelperClasses;

namespace Bss.RandomVariables.Contracts.Calculations
{
    /// <summary>
    /// Содержит методы для определения параметров функций
    /// </summary>
    public static class FunctionDetector
    {
        /// <summary>
        /// Определение коэффициентов линейного уравнения по 2 точкам
        /// </summary>
        /// <param name="point1">Точка 1</param>
        /// <param name="point2">Точка 2</param>
        /// <returns></returns>
        public static Tuple<double, double> Linear(TableValue point1, TableValue point2)
        {
            var k = (point2.ValueY - point1.ValueY)/(point2.ValueX - point1.ValueX);
            var b = (point2.ValueX * point1.ValueY - point1.ValueX * point2.ValueY)/(point2.ValueX - point1.ValueX);

            return new Tuple<double, double>(k, b);
        }
    }
}