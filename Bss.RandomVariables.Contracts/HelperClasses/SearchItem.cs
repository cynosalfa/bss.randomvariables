﻿using System;

namespace Bss.RandomVariables.Contracts.HelperClasses
{
    /// <summary>
    /// Искомое значение в интервале
    /// </summary>
    public class SearchItem
    {
        /// <summary>
        /// Левая граница интервала
        /// </summary>
        public double LeftEdge { get; set; }

        /// <summary>
        /// Правая граница интервала
        /// </summary>
        public double RightEdge { get; set; }

        /// <summary>
        /// Искомое значение
        /// </summary>
        public double SearchValue { get; private set; }

        /// <summary>
        /// Конструктор инициализации
        /// </summary>
        /// <param name="leftEdge">Левая границца интервала</param>
        /// <param name="rightEdge">Правая граница интервала</param>
        /// <param name="searchValue">Искомое значение</param>
        public SearchItem(double leftEdge, double rightEdge, double searchValue)
        {
            LeftEdge = leftEdge;
            RightEdge = rightEdge;
            SearchValue = searchValue;
        }

        /// <summary>
        /// Определяет, находится ли искомое значение в интервале
        /// </summary>
        /// <param name="func">Делегат</param>
        /// <returns>Логическое значение присутствия значения в интервале</returns>
        public bool IsThereRoot(Func<double, double> func)
        {
            var leftValue = func(LeftEdge);
            var rightValue = func(RightEdge);

            return (leftValue <= SearchValue && SearchValue <= rightValue) ||
                   (rightValue <= SearchValue && SearchValue <= leftValue);
        }
    }
}